﻿function Test1()
{
  var a,b,i,sset,c;
  var checkedArray = [];
  Browsers.Item(btChrome).Navigate("https://mpigovtnz-test1.cwp.govt.nz/news-and-resources/subscribe-to-mpi/abce4b4bb94b5d9545412ea0f96a4faf");
  sset = Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.formUpdatesubscriptionsform.fieldset.panelConsultations.panelConsultationcategories;
  let panel = Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.formUpdatesubscriptionsform.fieldset.panelConsultations;
  panel.Click();
  a=sset.ChildCount;
  Log.Message(a);
  //Updating Consulatation Categories list
  b = sset.EvaluateXPath("//*[contains(@id,'ConsultationCategories')][@type='checkbox']").toArray();
  c= sset.EvaluateXPath("//*[contains(@for,'ConsultationCategories')]").toArray();
  Log.Message(b);
  Log.Message(c[0].innerText);
  if (!strictEqual(b, null))
   {
     // and click the elements that were found 
     for (i=0;i<b.length;i++){
       if(i%2===1) {
        if(b[i].Checked == true)
         b[i].ClickChecked(false);
        else 
         b[i].ClickChecked(true);
      }
     if(b[i].Checked == true)
      {
        checkedArray.push(c[i].innerText);
        Log.Message(checkedArray);
      }
      }
   }
   else 
   { 
     // If nothing was found, post a message to the log 
     Log.Error("Nothing was found.");
   }
   Log.Message(checkedArray.length);
   let submitButton = Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.formUpdatesubscriptionsform.submitbuttonUpdate;
  aqObject.CheckProperty(submitButton, "value", cmpEqual, "Update");
  submitButton.ClickButton();
  WebTesting.Subscriptionlist.Check();
  let panel1 = NameMapping.Sys.browser.pageSubscribeToMpi2.panelPageWrapper.panel.panel.panel.panel.panel;
  let textNode = panel1.textnode;
  textNode.Click();
  var sset1, k,j;
  sset1 = Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.textnodeMainContent;
  k = sset1.EvaluateXPath("//h4/following-sibling::li").toArray();
  Log.Message(k.length);
  var dispArray = [];
  for(j=0;j<k.length;j++){
  dispArray.push(k[j].innerText);
  Log.Message(dispArray);
  }
  if (JSON.stringify(checkedArray) === JSON.stringify(dispArray)) {
    Log.Message('They are equal!');
  
}

}

function Test2()
{
  Browsers.Item(btChrome).Navigate("https://mpigovtnz-test1.cwp.govt.nz/at-expandable-page/"); 
  var expset,x,y;
  expset=Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList;
  x = expset.EvaluateXPath("//*[@class='processStep__title']");
  if(x[0].exists)
  Log.Message(x[0].innerText);
  x[0].Click();
  y = expset.EvaluateXPath("//*[@class='process-step non-numbered processStep open']");
  if(y[0].exists)
  aqObject.CheckProperty(expset.section.panel, "className", cmpEqual, "process-detail");
  Log.Message(expset.section.panel.contentText);
  let panel = Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList.panel;
  aqObject.CheckProperty(panel, "contentText", cmpEqual, "Expand All");
  panel.Click();
  panel = Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList.panel;
  aqObject.CheckProperty(panel, "contentText", cmpEqual, "Collapse All");
  panel.Click(1120, 4);
  
}

function Test3()
{
  aqObject.CheckProperty(Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetCheckboxGroup.textboxEnterYourName, "Name", cmpEqual, "Textbox(\"UserForm_Form_EditableTextField\")");
  let form = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm;
  let fieldset = form.fieldsetCheckboxGroup;
  aqObject.CheckProperty(fieldset.emailinputEnterYourEmail, "Name", cmpEqual, "EmailInput(\"UserForm_Form_EditableTextField_d8dd2\")");
  aqObject.CheckProperty(fieldset.fileUserformFormEditabletextfiel, "Name", cmpEqual, "File(\"UserForm_Form_EditableTextField\")");
  aqObject.CheckProperty(fieldset.fieldsetCheckboxGroup, "Name", cmpEqual, "Fieldset(\"EditableTextField_664b1\")");
  aqObject.CheckProperty(fieldset.panelEditableformstep1abaf.fieldsetRadioGroup, "Name", cmpEqual, "Fieldset(\"EditableTextField\")");
  let panel = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel;
  panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetCheckboxGroup.fileUserformFormEditabletextfiel.Click(76, 27);
  panel.panel2.panel.panel.panel.panel.panel.panel.panel.Click(203, 26);
  let browser = Aliases.browser;
  OCR.Recognize(browser.BrowserWindow).BlockByText("defined", spLeftMost).Click();
  browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetCheckboxGroup.fileUserformFormEditabletextfiel.Click(53, 19);
  aqObject.CheckProperty(Aliases.browser.dlgOpen, "Name", cmpEqual, "Window(\"#32770\", \"Open\", 1)");
  aqObject.CheckProperty(Aliases.browser.dlgOpen, "WndCaption", cmpEqual, "Open");
  fieldset = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetCheckboxGroup.fieldsetCheckboxGroup;
  aqObject.CheckProperty(fieldset.checkboxCheckbox1, "checked", cmpEqual, true);
  let checkbox = fieldset.checkboxCheckbox2;
  aqObject.CheckProperty(checkbox, "checked", cmpEqual, false);
  aqObject.CheckProperty(checkbox, "className", cmpEqual, "checkbox");
  checkbox.ClickChecked(true);
  form = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm;
  fieldset = form.fieldsetCheckboxGroup.panelEditableformstep1abaf.fieldsetRadioGroup;
  let radioButton = fieldset.radiobuttonGroup1;
  aqObject.CheckProperty(radioButton, "wChecked", cmpEqual, false);
  radioButton.ClickButton();
  aqObject.CheckProperty(radioButton, "checked", cmpEqual, true);
  radioButton = fieldset.radiobuttonGroup2;
  aqObject.CheckProperty(radioButton, "checked", cmpEqual, false);
  radioButton.ClickButton();
  aqObject.CheckProperty(radioButton, "checked", cmpEqual, true);
  let resetButton = form.resetbuttonClearTheForm;
  aqObject.CheckProperty(resetButton, "className", cmpEqual, "resetformaction");
  resetButton.ClickButton(); 
  
}

function Test4(){
  let table = Aliases.browser.pageAcceptableUsagePolicy.panel.form.table.table;
  if (table.Exists){
  aqObject.CheckProperty(table.cell, "contentText", cmpEqual, "Acceptable Use Policy (AUP) Agreement");
  let button = table.buttonIAccept;
  aqObject.CheckProperty(button, "value", cmpEqual, "I Accept");
  button.ClickButton();
  }
  browser.pageMpiNz.Wait();
}



