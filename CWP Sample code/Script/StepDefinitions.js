﻿let docsetarr_1, docsetarr_2, docsetarr_3, docfilter,articlearr,filtertext;
var checkedArray = [];
//The below line will enable us to include functions from other Script File
var unit1obj = require("GenFunc");
var unit2obj = require("WIN2Djourney");

BeforeFeature(function (){
  Browsers.Item(btChrome).Run("https://mpigovtnz-test1.cwp.govt.nz/");
  let browser = Aliases.browser;
  let page = browser.pageHome;
  if ((Aliases.browser.pageHome.Login).Exists	){
  page.Keys("[BS]test.gomez@mpi.govt.nz[Tab][BS]Welcome1");
  page.Login.buttonOk.ClickButton();
  let page2 = browser.pageLogin;
  page2.Wait();
  let form = page2.formMemberloginformLoginform;
  let fieldset = form.fieldset;
  let textbox = fieldset.textboxEmailRequired;
  textbox.Click();
  textbox.SetText("test.gomez@mpi.govt.nz");
  let passwordBox = fieldset.passwordboxPasswordRequired;
  passwordBox.Click();
  passwordBox.SetText("Welcome1");
  form.submitbuttonLogIn.ClickButton();
  }
  page.Wait();
    
  //Browsers.Item(btChrome).Run("https://mpigovtnz-test1.cwp.govt.nz/");
}
)

Given("I have launched the CWP website {arg}", function (param1){
  Browsers.Item(btChrome).Navigate("https://mpigovtnz-test1.cwp.govt.nz/");
  Aliases.browser.pageMpigovtnzTest1CwpGovtNz.Wait();
});

When("I click the {arg} tile", function (param1){
  let browser = Aliases.browser;
  if (param1 == "Biosecurity"){
    var biosec = browser.pageHome.FindChildByXPath("//*[@class='row branch-buttons-list']//a[contains(@href,'biosecuritynz')]");
  //aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel.panel.panel.panel.panel.link.aside,"className", cmpEqual, "branchButton branch-biosec branch-border");
   aqObject.CheckProperty(biosec, "contentText", cmpEqual, "Biosecurity New Zealand\nTiakitanga Pūtaiao Aotearoa");
    biosec.Click();
  browser.pageMpigovtnzTest1CwpGovtNzBiose.Wait();
  }
  if (param1 == "Fisheries"){
    var fisheries = browser.pageHome.FindChildByXPath("//*[@class='row branch-buttons-list']//a[contains(@href,'fisheriesnz')]");
  aqObject.CheckProperty(fisheries, "contentText", cmpEqual, "Fisheries New Zealand\nTini a Tangaroa");
  fisheries.Click();
  browser.pageMpigovtnzTest1CwpGovtNzFishe.Wait();
  }
  if (param1 == "Food Safety"){
    var foodsafety = browser.pageHome.FindChildByXPath("//*[@class='row branch-buttons-list']//a[contains(@href,'nzfoodsafety')]");
  aqObject.CheckProperty(foodsafety, "contentText", cmpEqual, "New Zealand Food Safety\nHaumaru Kai Aotearoa");
  foodsafety.Click();
  browser.pageMpigovtnzTest1CwpGovtNzNzfoo.Wait();  
  }
  if (param1 == "Forestry"){
    var forestry = browser.pageHome.FindChildByXPath("//*[@class='row branch-buttons-list']//a[contains(@href,'te-uru-rakau')]");
  aqObject.CheckProperty(forestry, "contentText", cmpEqual, "Te Uru Rākau\nForestry New Zealand");
  forestry.Click();
  browser.pageMpigovtnzTest1CwpGovtNzTeUru.Wait();  
  }
  if (param1 == "AIS"){
    var ais = browser.pageHome.FindChildByXPath("//*[@class='row branch-buttons-list']//a[contains(@href,'ais-branch')]");
    aqObject.CheckProperty(ais, "contentText", cmpContains, "AIS Branch");
   ais.Click();
   browser.pageMpigovtnzTest1CwpGovtNzNewBr.Wait();
  }
});

Then("the {arg} page is being displayed", function (param1){
  let browser = Aliases.browser;
  let panel = browser.pageHome;
  if (param1 == "Biosecurity"){
  WebTesting.Biosecurity.Check();
  Aliases.browser.pageMpigovtnzTest1CwpGovtNzBiose.header.navUtilitynav.link.Click();
  panel.Wait();
  }  
  if (param1 == "Fisheries"){  
  WebTesting.Fisheries.Check();
  browser.pageMpigovtnzTest1CwpGovtNzFishe.header.navUtilitynav.link.Click();
  panel.Wait();
  }
  if (param1== "Food Safety"){
  WebTesting.FoodSafety.Check();
  browser.pageMpigovtnzTest1CwpGovtNzNzfoo.header.navUtilitynav.link.Click();
  panel.Wait();  
  }
  if (param1 == "Forestry"){
  WebTesting.Forestry.Check();
  browser.pageMpigovtnzTest1CwpGovtNzTeUru.header.navUtilitynav.link.Click();
  panel.Wait();   
  }
  if(param1 == "AIS"){
  WebTesting.AISPage.Check(); 
  browser.pageMpigovtnzTest1CwpGovtNzNewBr.header.navUtilitynav.link.Click();
  panel.Wait();
  }
});

Given("I click Subscribe to MPI", function (){
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel.section.link, "contentText", cmpEqual, "Subscribe to MPI");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel.section.link, "namePropStr", cmpEqual, "subscribe-to-mpi");
  Aliases.browser.pageHome.panelBanner.panel.panel.panel3.section.link.Click();
});

When("I enter the Email and click Manage Subscriptions", function (){
  let page = Aliases.browser.pageSubscribeToMpiMpiNzGovernmen;
  page.Wait();
  WebTesting.SubscribetoMPI.Check();
  let form = page.formSubscriberdetailsform;
  let emailInput = form.fieldset.emailinputEmailRequired;
  emailInput.Click();
  emailInput.SetText("asdf@sdf.fds");
  form.submitbuttonManageSubscription.ClickButton();
  Aliases.browser.pageEmailed.Wait(); 
  WebTesting.Subscriptioninboxchk.Check();
});

When("I receive an email for subscription", function (){
  Browsers.Item(btChrome).Run("https://email.mpi.govt.nz/owa/cwp@mpi.govt.nz/?offline=disabled#path=/mail");
  let browser = Aliases.browser;
  let form = browser.pageOutlook.formLogonform;
  let textbox = form.textboxUsername;
  textbox.Click();
  textbox.SetText("network\\RajendrP");
  textbox.Keys("[Tab]");
  let panel = form.panel;
  panel.passwordboxPassword.SetText("Sarala@23456");
  panel.panel.textnode.Click();
  Delay(10000);
  browser.pageMailCwpMpiGovtNz.Wait();
  panel = Aliases.browser.pageMailCwpMpiGovtNz.panelPrimarycontainer.panel.panel.panel.panel.panel.panelAriaid27.panel;
  aqObject.CheckProperty(panel.panel.textnode, "contentText", cmpEqual, "securemail@datacomtaas.co.nz");
  panel.panel2.panel.textnode.Click();
  Delay(3000);
  let cell = NameMapping.Sys.browser.pageMailCwpMpiGovtNz.table.cell;
  let textNode = cell.textnode;
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "MPI subscriptions [addressed to asdf@sdf.fds]");
  textNode.Click(); 
  Delay(10000);
});

Then("I click on Subscription link", function (){
  //let cell = Aliases.browser.pageMailCwpMpiGovtNz.table.cell;
  //NameMapping.Sys.browser.pageMailCwpMpiGovtNz.Maximize();
  let cell = Aliases.browser.pageMailCwpMpiGovtNz.table.cell;
  textNode = cell.textnode2;
  if(textNode.Exists)
  Log.Message("Text node exists");
  textNode.WaitProperty("contentText","Select topics or unsubscribe",30)
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "Select topics or unsubscribe");
  textNode.Click();  
});

Then("I subscribe and update to topics of my choice", function (){
  aqObject.CheckProperty(Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.formUpdatesubscriptionsform, "idStr", cmpEqual, "UpdateSubscriptionsForm");
  aqObject.CheckProperty(Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.formUpdatesubscriptionsform, "tagName", cmpEqual, "FORM");
  var a,b,i,sset,c;
  sset = Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.formUpdatesubscriptionsform.fieldset.panelConsultations.panelConsultationcategories;
  panel = Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.formUpdatesubscriptionsform.fieldset.panelConsultations;
  panel.Click();
  a=sset.ChildCount;
  Log.Message(a);
  b = sset.EvaluateXPath("//*[contains(@id,'ConsultationCategories')][@type='checkbox']").toArray();
  c= sset.EvaluateXPath("//*[contains(@for,'ConsultationCategories')]").toArray();
  Log.Message(b);
  Log.Message(c[0].innerText);
  if (!strictEqual(b, null))
   {
     // and click the elements that were found 
     for (i=0;i<b.length;i++){
       if(i%2===1) {
        if(b[i].Checked == true)
         b[i].ClickChecked(false);
        else 
         b[i].ClickChecked(true);
      }
     if(b[i].Checked == true)
      {
        checkedArray.push(c[i].innerText);
        Log.Message(checkedArray);
      }
      }
   }
   else 
   { 
     // If nothing was found, post a message to the log 
     Log.Error("Nothing was found.");
   }
  Log.Message(checkedArray.length);
  let submitButton = Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.formUpdatesubscriptionsform.submitbuttonUpdate;
  aqObject.CheckProperty(submitButton, "value", cmpEqual, "Update");
  submitButton.ClickButton();
  WebTesting.Subscriptionlist.Check();
  let panel1 = NameMapping.Sys.browser.pageSubscribeToMpi2.panelPageWrapper.panel.panel.panel.panel.panel;
  let textNode = panel1.textnode;
  textNode.Click();
  var sset1, k,j;
  sset1 = Aliases.browser.pageSubscribeToMpi2.panelPageWrapper.textnodeMainContent;
  k = sset1.EvaluateXPath("//h4/following-sibling::li").toArray();
  Log.Message(k.length);
  var dispArray = [];
  for(j=0;j<k.length;j++){
  dispArray.push(k[j].innerText);
  }
  Log.Message(dispArray);
  //Compare whether all topics in first page is visible in second page
  if (JSON.stringify(checkedArray) === JSON.stringify(dispArray)) {
    Log.Message('The subscription topics are compared and are equal!');
}  
 unit1obj.CheckBrowser();
});

When("I click on Confirmation link sent to my email", function (){
 Browsers.Item(btChrome).Run("https://email.mpi.govt.nz/owa/cwp@mpi.govt.nz/");
  let browser = Aliases.browser;
  let form = browser.pageOutlook.formLogonform;
  let textbox = form.textboxUsername;
  textbox.Click();
  textbox.SetText("network\\RajendrP");
  textbox.Keys("[Tab]");
  let panel = form.panel;
  panel.passwordboxPassword.SetText("Sarala@23456");
  panel.panel.textnode.Click();
  browser.pageMailCwpMpiGovtNz.Wait();
  Delay(1000);
  let textNode = Aliases.browser.pageMailCwpMpiGovtNz.panelPrimarycontainer.panel.panel.panel.panel.panel.panelAriaid27.panel.panel.textnode;
  textNode.Click();
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "securemail@datacomtaas.co.nz");
  let cell = NameMapping.Sys.browser.pageMailCwpMpiGovtNz.table.cell;
  textNode = cell.link.textnode;
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "Action required: Confirm your subscription [addressed to asdf@sdf.fds]");
  textNode.Click();
  textNode = cell.textnode2;
  textNode.WaitProperty("contentText","Complete and confirm my subscription",5)
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "Complete and confirm my subscription");
  textNode.Click();
});

Then("I see the subscription has successfully updated", function (){
  WebTesting.SubscriptionConfirmation.Check();
  Delay(3000);
  Log.Message("Subscription list has been updated");
  unit1obj.CheckBrowser();
});

Given("I have navigated to {arg} page", function (param1){
  let browser = Aliases.browser;
  if (param1 == "Food Safety"){
  let textNode = browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.link.textnode;
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "Food safety");
  textNode.Click();
  browser.pageMpigovtnzTest1CwpGovtNzFoodS.Wait();
  }
  if (param1 == "Expandable Page"){
  Browsers.Item(btChrome).Navigate("https://mpigovtnz-test1.cwp.govt.nz/new-image-sub-landing-page/at-expandable-page/"); 
 // browser.pageMpigovtnzTest1CwpGovtNzAtExp.Wait();
  aqObject.CheckProperty(Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList, "className", cmpEqual, "process-flow expandable has-upper-content");
  }
  if (param1 == "Docset"){
   Browsers.Item(btChrome).Navigate("https://mpigovtnz-test1.cwp.govt.nz/news-and-resources/publications/");  
  }
  if (param1 == "Userforms"){
  let page = browser.pageHome;
  let textNode = page.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.link3.textnode;
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "Test User defined form page");
  textNode.Click();
  browser.pageMpigovtnzTest1CwpGovtNzTestU.Wait();
  WebTesting.Userformspage.Check();
  }
  if(param1 == "WIN2D"){
   Browsers.Item(btChrome).Navigate("https://mpigovtnz-test1.cwp.govt.nz/food-safety/food-act-2014/win2d-questions/");   
  }
  if (param1 == "Pig Space Calculator"){
    Browsers.Item(btChrome).Navigate("https://mpigovtnz-test1.cwp.govt.nz/new-branch-landing-page/at-test-pig-space-calculator/");
  }
  if (param1 == "FLCR Page"){
    Browsers.Item(btChrome).Navigate("https://mpigovtnz-test1.cwp.govt.nz/fisheriesnz/fisheries-levy-cost-recovery-page/");
  }
 }); 

When("I click on the Feedback bubble", function (){
  Regions.link.Check(Aliases.browser.pageMpigovtnzTest1CwpGovtNzFoodS.panelFeedbacklink.link);
  let svg = Aliases.browser.pageMpigovtnzTest1CwpGovtNzFoodS.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panelFeedbacklink.link.vgLayer1;
  aqObject.CheckProperty(svg, "ObjectType", cmpEqual, "SVG");
  svg.Click();
  WebTesting.Feedbackform.Check();
});

When("when I enter the Feedback form details", function (){
  let form = Aliases.browser.pageMpigovtnzTest1CwpGovtNzFoodS.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm;
  let fieldset = form.fieldsetHowSatisfiedWereYouWithO;
  fieldset.fieldsetHowSatisfiedWereYouWithO.label.Click();
  let textarea = fieldset.textareaGiveUsYourFeedback;
  textarea.Click();
  textarea.Keys("Testing");
  aqObject.CheckProperty(textarea, "ObjectLabel", cmpEqual, "Give us your feedback");
  aqObject.CheckProperty(fieldset.emailinputDoYouWantAReply, "ObjectLabel", cmpEqual, "Do you want a reply?");
  let emailInput = Aliases.browser.pageMpigovtnzTest1CwpGovtNzFoodS.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetHowSatisfiedWereYouWithO.emailinputDoYouWantAReply;
  aqObject.CheckProperty(emailInput, "idStr", cmpEqual, "UserForm_Form_Emailaddress");
  emailInput.Click();
  emailInput.SetText("abc@xyz.com");
  //Cannot add code for recaptcha
  let submitButton = form.submitbuttonSubmit;
  aqObject.CheckProperty(submitButton, "ObjectLabel", cmpEqual, "Submit");
  aqObject.CheckProperty(submitButton, "Enabled", cmpEqual, true);
  form.submitbuttonSubmit.ClickButton();
});

Then("I can see Thank you for feedback page", function (){
  throw new NotImplementedError();
});

When("I click on each Expandable Item", function (){
  var expset,x;
  expset=Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList;
  x = expset.EvaluateXPath("//*[@class='processStep__title']");
  if(x[0].exists)
  Log.Message(x[0].innerText);
  x[0].Click();
});

Then("I should be able to view the content inside the expandable", function (){
  var expset,y;
  expset=Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList;
  y = expset.EvaluateXPath("//*[@class='process-step non-numbered processStep open']");
  if(y[0].exists)
  aqObject.CheckProperty(expset.section.panel, "className", cmpEqual, "process-detail");
  Log.Message(expset.section.panel.contentText);
});

When("I click on Expand All button", function (){
 let panel = Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList.panel;
  aqObject.CheckProperty(panel, "contentText", cmpEqual, "Expand All");
  panel.Click();
});

Then("all the Expandable Items should be expanded", function (){
  var expset,x,y,i;
  expset=Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList;
  y = expset.EvaluateXPath("//*[@class='processStep__title']");
  //for every expandable item in the page, the 
  for(i=0;i<y.length;i++)
  {
    x = expset.EvaluateXPath("//*[@class='process-step non-numbered processStep closed']");
    if(strictEqual(x, null))
    Log.Message("All the expandable items are expanded");
  }
});

When("I Click on Collapse All buuton", function (){
panel = Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList.panel;
  aqObject.CheckProperty(panel, "contentText", cmpEqual, "Collapse All");
  panel.Click();
});

Then("all the opened expandable items should close", function (){
  var expset,x,y,i;
  expset=Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelArticleList;
  y = expset.EvaluateXPath("//*[@class='processStep__title']");
  //for every expandable item in the page, the 
  for(i=0;i<y.length;i++)
  {
    x = expset.EvaluateXPath("//*[@class='process-step non-numbered processStep open']");
    if(strictEqual(x, null))
    Log.Message("All the expandable items are closed");
  }
});

When("I click on a topic in Bookmark link", function (){
  var x;
  panel = Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel;
  aqObject.CheckProperty(panel.textnode, "contentText", cmpEqual, "On this page:");
  //panel.link.Click();
  x = panel.EvaluateXPath("//ul[@class='bookmarks']//a[contains(@href, '#')]")
  Log.Message("Click the bookamrk link");
  if(!strictEqual(x, null)){
  x[0].Click();
  }
});

Then("it should take me to the related Expandable Set", function (){
  var panel,x,y,i;
  panel = Aliases.browser.pageMpigovtnzTest1CwpGovtNzAtExp.panelPageWrapper.panel.panel.panel.panel.panel.panel;
  x = panel.EvaluateXPath("//ul[@class='bookmarks']//a[contains(@href, '#')]")
  //y = panel.EvaluateXPath("//*[@class='process-flow expandable has-upper-content']/h2/a")
  //code for checking scroll functionality
  y = panel.EvaluateXPath("//*[@class='process-flow expandable has-upper-content']/h2")
  for(i=0;i<y.length;i++)
  {
    if(y[i].Focused)
   {
      var b = panel.querySelector("h2");
      if (y[i].contentText == x[0].contentText){
        b.onscroll = logScroll();
       function logScroll(){
          Log.Message("Scrolled to required position:" + y[i].scrollHeight);
       }
      Log.Message("The bookmark link of the page is in focus - " + y[i].contentText);
    }
    }
  }
});

When("I click anywhere in the title to open a document", function (){
  var panel,x;
  let browser = Aliases.browser;
  panel = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.panelArticleList7
  x = panel.EvaluateXPath("//div[@class='dmsDocument__title']");
  if(x[0].exists)
  Log.Message(x[0].innerText);
  x[0].Click();
});

Then("the document content should be expanded", function (){
  var x,panel;
  panel = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.panelArticleList7
  x = panel.FindChildByXPath("//article[@class='toggle dms-document  dmsDocument open']");
  if(x.exists)
  Log.Message(x.contentText);
});

When("I click the Download button", function (){
  let browser = Aliases.browser;
  let link = browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.panelArticleList7.article.linkDmsdocumentdownloadlink871;
  aqObject.CheckProperty(link, "className", cmpEqual, " button button--primary dmsDocument__download");
  link.textnode.Click();
});

Then("clicking again should close the expanded content", function (){
  var x,y,panel;
  panel = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.panelArticleList7
  x = panel.EvaluateXPath("//div[@class='dmsDocument__title']");
  if(x[0].exists)
  Log.Message(x[0].innerText);
  x[0].Click();
  y = panel.FindChildByXPath("//article[@class='toggle dms-document  dmsDocument closed']");
  if(y.exists)
  Log.Message("The expanded content is closed");
});

Then("the document should open in new tab", function (){
 let browser = Aliases.browser;
 let link = browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.panelArticleList7.article.linkDmsdocumentdownloadlink871
 var url = link.href;
 if (Sys.Browser().WaitPage(url, 0).Exists)
 Log.Message("The required document has been opened in another tab successfully"); 
 Delay(2000);
 Sys.Browser().Page(url).Close();
 Log.Message("The document has been closed");
});

When("I click Load more", function (){
  docsetarr_1 = unit1obj.getArraylength();
  Log.Message(docsetarr_1);
  let textNode = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.textnode;
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "Load more documents");
  textNode.Click();
  Delay(5000);
});

Then("the document set items are loaded", function (){
  docsetarr_2 = unit1obj.getArraylength();
  Log.Message(docsetarr_2);
  if(docsetarr_1 < docsetarr_2)
  Log.Message("The documents are loaded successfully");
});

When("I click Expand All button in Doc set", function (){
 var panel,x;
 panel = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.panelArticleList7;
 x = panel.FindChildByXPath("//div[@class='toggle-master toggle-master-open']");
 if (x.contentText == "Expand All")
 x.Click(); 
});

Then("the document set items that are loaded is also expanded", function (){
  var panel,x,y,i;
  panel = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.panelArticleList7;
  y = panel.EvaluateXPath("//div[@class='dmsDocument__title']");
  //for every expandable item in the page, the 
  for(i=0;i<y.length;i++)
  {
    x = panel.EvaluateXPath("//article[@class='toggle dms-document  dmsDocument closed']");
    if(strictEqual(x, null))
    Log.Message("All the documents after Load more are expanded");
    else
    Log.Error("The documents are not expanded after Load More");
  }
  panel.FindChildByXPath("//div[@class='toggle-master']").Click();
});

When("I search for a document using valid Keyword and click Submit", function (){
  docsetarr_3 = unit1obj.getArraylength();
  Log.Message(docsetarr_3);
  let fieldset = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.formFormDocumentsetfilter.fieldset;
  aqObject.CheckProperty(fieldset, "contentText", cmpStartsWith, "Tips");
  let textbox = fieldset.textboxFilterByTitleKeywordOrDes;
  aqObject.CheckProperty(textbox, "ObjectLabel", cmpEqual, "Filter by title, keyword, or description");
  textbox.Click();
  textbox.SetText("Reports");
  let vselect = fieldset.selectDocumentType;
  aqObject.CheckProperty(vselect, "ObjectLabel", cmpEqual, "Document type");
  vselect.ClickItem("Report");
  textbox = fieldset.textboxPublishedAfter;
  aqObject.CheckProperty(textbox, "ObjectLabel", cmpEqual, "Published after");
  textbox.Click();
  textbox.Keys("30102015");
  let button = fieldset.buttonSubmit;
  aqObject.CheckProperty(button, "contentText", cmpEqual, "Submit");
  button.ClickButton();
  Delay(2000);
});

Then("it should result in the list of documents matching the Keyword", function (){
  docfilter = unit1obj.getArraylength();
  Log.Message(docfilter);
  if (docsetarr_3 !=0){
    try
{
    if (docsetarr_3 > docfilter)
    Log.Message("The filter functionality works fine!");
}
catch (e)
{
  Log.Error("Check the code");
}  
  }
});

When("I click Reset", function (){
  let button = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.formFormDocumentsetfilter.fieldset.panelDocumentsetfilter.panel.buttonReset;
  aqObject.CheckProperty(button, "contentText", cmpEqual, "Reset");
  button.ClickButton();
});


Then("the Search field should be empty", function (){
  var arrsetreset;
  arrsetreset = unit1obj.getArraylength();
  let fieldset = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.formFormDocumentsetfilter.fieldset;
  let panel = fieldset.panelDocumentsetfilter.panel;
  try{
  if (docfilter < arrsetreset){
  aqObject.CheckProperty(fieldset.textboxFilterByTitleKeywordOrDes, "value", cmpEqual, "");
  aqObject.CheckProperty(fieldset.selectDocumentType, "wText", cmpEqual, "All");
  aqObject.CheckProperty(fieldset.textboxPublishedAfter, "value", cmpEqual, "");
  aqObject.CheckProperty(panel.textboxPublishedBefore, "value", cmpEqual, "");
  }
  }
  catch (e){
  Log.Error("The reset functionality didn't work");
  }
});

When("I search for a document using invalid Keyword and click Submit", function (){
  let fieldset = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.formFormDocumentsetfilter.fieldset;
  aqObject.CheckProperty(fieldset, "contentText", cmpStartsWith, "Tips");
  let textbox = fieldset.textboxFilterByTitleKeywordOrDes;
  aqObject.CheckProperty(textbox, "ObjectLabel", cmpEqual, "Filter by title, keyword, or description");
  textbox.Click();
  textbox.SetText("qqqq");
  let button = fieldset.buttonSubmit;
  aqObject.CheckProperty(button, "contentText", cmpEqual, "Submit");
  button.ClickButton();
  Delay(2000);
});

Then("it should not populate any results", function (){
  var emptyarr = unit1obj.getArraylength();
  if (strictEqual(emptyarr, null)){
  aqObject.CheckProperty(Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.textnode, "contentText", cmpEqual, "Sorry, no documents were found. If you are using the filter please try different values.");
  Log.Message("Invalid Keyword returns no result");
 } 
});

When("I enter the related details and Click Clear form", function (){
  let fieldset = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetCheckboxGroup;
  aqObject.CheckProperty(fieldset.label, "contentText", cmpEqual, "Enter your name");
  let textbox = fieldset.textboxEnterYourName;
  aqObject.CheckProperty(textbox, "Name", cmpEqual, "Textbox(\"UserForm_Form_EditableTextField\")");
  textbox.Click();
  textbox.SetText("XYZ");
  aqObject.CheckProperty(fieldset.label2, "contentText", cmpEqual, "Enter your Email");
  let emailInput = fieldset.emailinputEnterYourEmail;
  aqObject.CheckProperty(emailInput, "Name", cmpEqual, "EmailInput(\"UserForm_Form_EditableTextField_d8dd2\")");
  emailInput.Click(40, 18);
  emailInput.SetText("abc@xyz.com");
  aqObject.CheckProperty(fieldset.fileUserformFormEditabletextfiel, "Name", cmpEqual, "File(\"UserForm_Form_EditableTextField\")");
  uploadfileObj = fieldset.FindChildByXPath("//input[@type='file']");
  uploadfileObj.Click();
  //To upload file
  dlgFileToUpload = Aliases.browser.dlgOpen
  dlgFileToUpload.Window("ComboBoxEx32").SetText("C:\\Users\\RajendrP\\Desktop\\Testfile\\babies.jpg");
  dlgFileToUpload.Window("Button", "&Open").ClickButton();
  aqObject.CheckProperty(fieldset.fieldsetCheckboxGroup, "Name", cmpEqual, "Fieldset(\"EditableTextField_664b1\")");
  aqObject.CheckProperty(fieldset.fieldsetCheckboxGroup.checkboxCheckbox1, "checked", cmpEqual, true);
  Log.Message("The default checkbox is checked");
  let checkbox = fieldset.fieldsetCheckboxGroup.checkboxCheckbox2;
  aqObject.CheckProperty(checkbox, "checked", cmpEqual, false);
  aqObject.CheckProperty(checkbox, "className", cmpEqual, "checkbox");
  checkbox.ClickChecked(true);
  Log.Message("Checkbox can be clicked");
  aqObject.CheckProperty(fieldset.panelEditableformstep1abaf.fieldsetRadioGroup, "Name", cmpEqual, "Fieldset(\"EditableTextField\")");
  let radioButton1 = fieldset.panelEditableformstep1abaf.fieldsetRadioGroup.radiobuttonGroup1;
  aqObject.CheckProperty(radioButton1, "wChecked", cmpEqual, false);
  radioButton1.ClickButton();
  aqObject.CheckProperty(radioButton1, "checked", cmpEqual, true);
  radioButton2 = fieldset.panelEditableformstep1abaf.fieldsetRadioGroup.radiobuttonGroup2;
  aqObject.CheckProperty(radioButton2, "checked", cmpEqual, false);
  radioButton2.ClickButton();
  aqObject.CheckProperty(radioButton2, "checked", cmpEqual, true);
  try{
  if (radioButton1.checked == false)
  Log.Message("Only one option can be selected");
  }
  catch (e){
    Log.Error("only option selection property is not working");
  }
  form = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm;
  let resetButton = form.resetbuttonClearTheForm;
  aqObject.CheckProperty(resetButton, "className", cmpEqual, "resetformaction");
  resetButton.ClickButton();
  Delay(5000);
});

Then("I should be able to see that all the data are erased", function (){
  let fieldset = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetCheckboxGroup;
  aqObject.CheckProperty(fieldset.textboxEnterYourName, "value", cmpEqual, "");
  aqObject.CheckProperty(fieldset.emailinputEnterYourEmail, "value", cmpEqual, "");
  aqObject.CheckProperty(fieldset.fileUserformFormEditabletextfiel, "value", cmpEqual, "");
  radioButton2 = fieldset.panelEditableformstep1abaf.fieldsetRadioGroup.radiobuttonGroup2;
  aqObject.CheckProperty(radioButton2, "checked", cmpEqual, false);
  Log.Message("All the data are erased");
});

When("I enter mandatory field details", function (){
  let fieldset = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetCheckboxGroup;
  aqObject.CheckProperty(fieldset.label, "contentText", cmpEqual, "Enter your name");
  let textbox = fieldset.textboxEnterYourName;
  aqObject.CheckProperty(textbox, "Name", cmpEqual, "Textbox(\"UserForm_Form_EditableTextField\")");
  textbox.Click();
  textbox.SetText("XYZ");
  aqObject.CheckProperty(fieldset.label2, "contentText", cmpEqual, "Enter your Email");
  let emailInput = fieldset.emailinputEnterYourEmail;
  aqObject.CheckProperty(emailInput, "Name", cmpEqual, "EmailInput(\"UserForm_Form_EditableTextField_d8dd2\")");
  emailInput.Click(40, 18);
  emailInput.SetText("abc@xyz.com");
  aqObject.CheckProperty(Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.submitbuttonSubmitTheForm, "ObjectLabel", cmpEqual, "Submit the Form");  
});

When("I delete the mandatory field", function (){
  let emailInput = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetCheckboxGroup.emailinputEnterYourEmail;
  emailInput.Click();
  emailInput.SetText("");
  Log.Message("The email(mandatory)field is now with no data");
});

When("I click Submit button", function (){
  let submitButton = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.submitbuttonSubmitTheForm;
  aqObject.CheckProperty(submitButton, "value", cmpEqual, "Submit the Form");
  submitButton.ClickButton();
});

Then("I should be able to see the error message", function (){
  var i;
  let field = Aliases.browser.pageMpigovtnzTest1CwpGovtNzTestU.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.formUserformForm.fieldsetCheckboxGroup.panelEditableformstep1abaf;
  let panel1 = field.panel;
  aqObject.CheckProperty(panel1.panel, "contentText", cmpContains, "Please correct the following errors and try again:");
  var errlist = panel1.panel.EvaluateXPath("//ul[@class='error-list']//li//a");
  Log.Message(errlist[0].contentText);
  var errmsg = field.panelEditabletextfieldD8dd2.panel.EvaluateXPath("//span[@id='UserForm_Form_EditableTextField_d8dd2-error']");
  Log.Message(errmsg[0].contentText);
  if(!strictEqual(errlist,null))
    {
      for (i=0;i<errlist.length;i++)
     {
      errlist[i].Click(); 
      try{
        if((errlist[i].contentText) == (errmsg[i].contentText))
        Log.Message("The required mandatory field: "+ errlist[i].contentText);
      }
      catch (e){
        Log.Error("Error list is not being properly displayed");
      }
    }
  }  
  
});

Then("I should be able to successfully submit the form", function (){
  aqObject.CheckProperty(Aliases.browser.pageTestUserDefinedFormPageMpiNz.panelPageWrapper.panel.panel.panel.panel.panel.panel.panelUff.textnode, "contentText", cmpEqual, "Thanks, we\'ve received your submission. You can see other screens");
});

Then("I close the Feedback form pop-up", function (){
  aqObject.CheckProperty(Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel.panel.panel.vg, "ObjectType", cmpEqual, "SVG");
  aqObject.CheckProperty(Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel.panel.panel.vg, "tagName", cmpEqual, "svg");
  Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel.panel2.panel.vg.Click();
});

When("I scroll to Media Releases section", function (){
 Delay(3000);
 panel = Aliases.browser.pageHome.panelPageWrapper.panel.panel.panel.section.panel;
 panel.scrollIntoView(true);
});

Then("I could see only three Media releases tile arranged by date and Category", function (){
  panel = Aliases.browser.pageHome.panelPageWrapper.panel.panel.panel.section;
  var arrnews,i,newsdate,newscat;
  arrnews = panel.EvaluateXPath("//a[@href]//article");
  newsdate = panel.EvaluateXPath("//a[@href]//article//dd[@class='date']");
  //Date conversion
  var a = aqConvert.StrToDate(newsdate[0].contentText);
  Log.Message(a);
  newscat = panel.EvaluateXPath("//a[@href]//article//dd[@class='news-category']");
  Delay(5000);
  Log.Message(arrnews.length);
  if((!strictEqual(arrnews.length,null))&&(arrnews.length<4)){
    Log.Message("Only 3 media Releases are shown on Home Page");
    for(i=3;i>0;i--){
    if(!strictEqual(newscat,null))
     Log.Message("The News Category is: " + newscat[i-1].contentText);
     try{
     if(i>1){
      var a = aqConvert.StrToDate(newsdate[i-2].contentText);
      var b = aqConvert.StrToDate(newsdate[i-1].contentText);
      if (a>b)
     Log.Message("News are arranged by dates")
     }
     }
     catch (e){
     Log.Error("Some confusion on Date display");
     }
    }
  }
});


Then("the abstract of the Media releases tile are trimmed to three dots after hundred characters", function (){
 let panel = Aliases.browser.pageHome.panelPageWrapper.panel.panel.panel.section;
 var par_arr,i;
 par_arr = panel.EvaluateXPath("//p[@class='mediaRelease__summary']")
   for(i=0;i<par_arr.length;i++){
     let aString = par_arr[i].contentText;
     Log.Message("The abstract of the News is: " + aString);
     if(aqString.GetLength(aString)>100){
     let aSubString = aString.substr(100);
     Log.Message("The characters after 100th character is: " + aSubString);
     if((aSubString.localeCompare("..."))== 0)
     Log.Message("The characters are trimmed after 100th character");
     }
   }
});

When("I click on View All Media Releases", function (){
  let browser = Aliases.browser;
  let link = browser.pageHome.panelPageWrapper.panel.panel.panel.section.panel.link;
  aqObject.CheckProperty(link, "contentText", cmpEqual, "View all media releases");
  link.Click();
  browser.pageMpigovtnzTest1CwpGovtNzNewsA.Wait();
  WebTesting.MediaReleases.Check();
});

Then("I could see a set of Media Releases topic arranged by Date with a Show more link", function (){
  let articleIndex = Aliases.browser.pageMpigovtnzTest1CwpGovtNzNewsA.panelNewsArticleIndex;
  var i,newscat,newsdate,newstitle;
  articlearr = articleIndex.EvaluateXPath("//article[@class]");
  newscat = articleIndex.EvaluateXPath("//article[@class]//dd[@class='news-category newsItem__title']");
  newsdate = articleIndex.EvaluateXPath("//article[@class]//dd[@class='date']");
  newstitle = articleIndex.EvaluateXPath("//article[@class]//h2[@class='newsItem__title']");
  for(i =0;i<articlearr.length;i++){
   if(!strictEqual(newscat,null))
     Log.Message("The News Category is: " + newscat[i].contentText + " And The Title and Date is: " + newstitle[i].contentText + newsdate[i].contentText);
     try{
     if(i<9){
      var a = aqConvert.StrToDate(newsdate[i].contentText);
      var b = aqConvert.StrToDate(newsdate[i+1].contentText);
      if (a>b)
     Log.Message("News are arranged by dates")
     }
     }
     catch (e){
     Log.Error("Some confusion on Date display");
     } 
  }
  aqObject.CheckProperty(Aliases.browser.pageMpigovtnzTest1CwpGovtNzNewsA.panelPageWrapper.panel.panel.panel.panel.panel.panel.link, "contentText", cmpEqual, "Show more");
  Log.Message("The Show More link is seen");
});

When("I click the Show more link", function (){
  let link = Aliases.browser.pageMpigovtnzTest1CwpGovtNzNewsA.panelPageWrapper.panel.panel.panel.panel.panel.panel.link;
  link.Click();
  Delay(5000);
});

Then("other set of Media Release topics are loaded", function (){
  let articleIndex = Aliases.browser.pageMpigovtnzTest1CwpGovtNzNewsA.panelNewsArticleIndex;
  var articlearr2 = articleIndex.EvaluateXPath("//article[@class]");
  Log.Message("The list after clicking load more is: " + articlearr2.length);
  if (articlearr.length<articlearr2.length)
  Log.Message("The next set of Media Release topics are loaded successfully");
  else
  Log.Error("The Load more functionality didn't work");
});

When("I click on a topic to filter media Releases by Category", function (){
  let panel = Aliases.browser.pageMpigovtnzTest1CwpGovtNzNewsA.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel;
  var arrtag = panel.EvaluateXPath("//div[@class='tag-list category']//a");
  arrtag[3].Click();
  filtertext = arrtag[3].contentText;
  Log.Message("The filter topic that was clicked is: " + filtertext);
  Delay(3000);
});

Then("I could see only the Media Release topics related to the category are displayed", function (){
 let articleIndex = Aliases.browser.pageMpigovtnzTest1CwpGovtNzNewsA.panelNewsArticleIndex; 
 var i;
 var newscat = articleIndex.EvaluateXPath("//article[@class]//dd[@class='news-category newsItem__title']");
 for (i=0;i<newscat.length;i++){
   if(newscat[i].contentText == filtertext)
   Log.Message("The topics are filtered as expected");
   else
   Log.Error("Check the filter function");
 }
});

When("I click on Start Page", function (){
 // WebTesting.WIN2DPage.Check();
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  let link = page.panelPageWrapper.panel.panel.link;
  aqObject.CheckProperty(link, "contentText", cmpEqual, "Start questionnaire");
  link.Click();
  page.Wait();
});

Then("I should be taken to WIN2D journey and see the progress bar start point", function (){
  WebTesting.WIN2dStartPage.Check();
  aqObject.CheckProperty(Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel, "className", cmpEqual, "win2d_master win2d");
  let panel = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel;
  aqObject.CheckProperty(panel.panel2.panel.panel.panel.panel.panel.form.submitbuttonNext, "value", cmpEqual, "Next");
  Log.Message("The next button is only available in front page");
  var progressbar = panel.FindChildByXPath("//div[@class='progress']");
  var progressbarpercent = panel.FindChildByXPath("//div[@class='progress']//div[@aria-valuenow]");
  if( aqObject.CompareProperty(progressbarpercent.outerHTML, cmpContains, 'aria-valuenow="0.72"')== true)
  Log.Message("The Progress bar is at the Starting position");
});

Then("I should be able to see the downward arrow to expand the question tile", function (){
   let panel = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel;
   var downarrow = panel.FindChildByXPath("//div[@class='win2dOption__readMoreIndicator__arrow']");
   downarrow.Click();
   Log.Message("The Question tile is expanded");
   downarrow.Click();
   Log.Message("The Question tile is shrinked");
});

When("I click Next button", function (){
  let submitButton = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.form.submitbuttonNext;
  aqObject.CheckProperty(submitButton, "value", cmpEqual, "Next");
  submitButton.ClickButton();
});

Then("I should be presented with error page", function (){
  aqObject.CheckProperty(Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.panel.panel.panel.panel, "contentText", cmpEqual, "Please choose at least one answer");
});

When("I see multi choice question", function (){
  let panel = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel;
  var multipara = panel.FindChildByXPath("//div[@class='win2d__question__subtitle']/p");
  aqObject.CheckProperty(multipara, "contentText", cmpEqual, "Select all the options that apply to you.");
});

Then("I should be able to select more than one answer", function (){
 let optionset = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel;
 var multichoice = optionset.EvaluateXPath("//div[@class='win2dOption ']");
 multichoice[0].Click();
 multichoice[1].Click();
 var multichoicechk = optionset.EvaluateXPath("//div[contains(@class,'selected')]");
 Log.Message(multichoicechk[0].contentText);
 if((multichoicechk[0].Exists)&&(multichoicechk[1].Exists))
 Log.Message("I can select two options at the same time");
 else
 Log.Error("Multiple options cannot be selected");
});

When("Navigate to single option question", function (){
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  let form = page.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.form;
  aqObject.CheckProperty(form.textnode, "contentText", cmpEqual, "What will you do?");
  let panel = form.panelWin2doption12985.panel.panel;
  aqObject.CheckProperty(panel, "contentText", cmpEqual, "Sell food to other businesses.");
  panel.Click();
  form.submitbuttonNext.ClickButton();
  page.Wait();
  aqObject.CheckProperty(form.textnode2, "contentText", cmpEqual, "Choose only one answer.");
});

Then("I should be able to select only one answer", function (){
 let optionset = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel;
 var singleoption = optionset.EvaluateXPath("//div[@class='win2dOption ']");
 singleoption[0].Click();
 Delay(3000);
 singleoption[1].Click();
 var singleoption = optionset.EvaluateXPath("//div[@class='win2dOption ']");
 if(strictEqual(singleoption,null))
 Log.Message("Only one option can be selected at a time");
 else
 Log.Error("Check the functionality!");
});

When("I click on Review answers link from second question", function (){
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  let link = page.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.panel.panel.panel.link;
  aqObject.CheckProperty(link, "contentText", cmpEqual, "Review answers");
  link.Click();  page.Wait();
});


Then("I should be able to see the Review answers page with all the questions I have answered so far", function (){
  panel = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel;
  aqObject.CheckProperty(panel.textnode, "contentText", cmpEqual, "Summary Review");
  answerset = panel.EvaluateXPath("//div[@class = 'win2d_answer-review']");
  if(!strictEqual(answerset,null))
  Log.Message("The questions that were answered so far can be seen");
  else
  Log.Error("Nothing in Review Page");
});


When("on the Review Page, I click reset", function (){
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  let panel = page.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.panel;
  var resetquestion = panel.EvaluateXPath("//a[@class = 'win2d_navigation-backToQuestion']");
  aqObject.CheckProperty(resetquestion[1], "contentText", cmpEqual, "Reset to this point");
  resetquestion[1].Click();
  page.Wait();
});

Then("I should be taken to the question where I click Reset", function (){
 aqObject.CheckProperty(Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.panel.textnode, "contentText", cmpStartsWith, "Back to question");
});

When("I click on Restart Questionnaire link", function (){
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  let panel = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.panel.panel.panel;
  restartlink = panel.FindChildByXPath("//a[@class = 'win2d_navigation-restart__link']")
  aqObject.CheckProperty(restartlink, "contentText", cmpEqual, "Restart questionnaire");
  restartlink.Click();
  page.Wait();
});

Then("I should be taken to the First Question Page", function (){
  aqObject.CheckProperty(Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.panel.textnode, "contentText", cmpEqual, "Your session has been cleared and you may start again");
});

When("I answer all the questions of the WIN2D journey", function (param1){
 unit2obj.win2d(); 
});

When("I could see Next button, back button, Review answers link, Restart Questionnaire link and the progress bar", function (){
  let panel = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel;
  aqObject.CheckProperty(panel.form.submitbuttonNext, "value", cmpEqual, "Next");
  let link = panel.panel.panel.panel.link;
  aqObject.CheckProperty(link, "contentText", cmpEqual, "Review answers");
  restartlink = panel.FindChildByXPath("//a[@class = 'win2d_navigation-restart__link']")
  aqObject.CheckProperty(restartlink, "contentText", cmpEqual, "Restart questionnaire");
  panel = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel;
  let backButton = panel.FindChildByXPath("//input[@class='win2d_navigation-back']");
  aqObject.CheckProperty(backButton, "value", cmpEqual, "Back");
  aqObject.CheckProperty(backButton, "Enabled", cmpEqual, true);
  aqObject.CheckProperty(panel.panel.panel.panel, "className", cmpEqual, "progress-bar");
  var progressbar = panel.FindChildByXPath("//div[@class='progress']");
  var progressbarpercent = panel.FindChildByXPath("//div[@class='progress']//div[@aria-valuenow]");
  if( aqObject.CompareProperty(progressbarpercent.outerHTML, cmpContains, 'aria-valuenow="99.28"')== true)
  Log.Message("All required links are seen and the progress bar is at completed stage")
});

Then("I should be able to sucessfully submit all my answers in the Review page", function (){
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  let form = page.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.form;
  let submitButton = form.submitbuttonNext;
  aqObject.CheckProperty(submitButton, "value", cmpEqual, "Next");
  submitButton.ClickButton();
  page.Wait();
  page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  let panel = page.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel;
  aqObject.CheckProperty(panel.textnode, "contentText", cmpEqual, "Summary Review");
  var nextbtn = panel.FindChildByXPath("//a[@class='action']");
  aqObject.CheckProperty(nextbtn, "contentText", cmpEqual, "Next");
  nextbtn.Click();
  page.Wait();
});

When("I go to Location Verification page", function (){
  aqObject.CheckProperty(Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.textnode, "contentText", cmpEqual, "Note: if you have more than one location, you can select multiple addresses one at a time.");
});

Then("I should see that the Next button is not enabled eithout entering the address", function (){
  let button = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.form.buttonNext;
  aqObject.CheckProperty(button, "contentText", cmpEqual, "Next");
  aqObject.CheckProperty(button, "Enabled", cmpEqual, false);
});

When("I enter location address", function (){
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  let textNode = page.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.panel.textnode;
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "Add an address");
  textNode.Click();
  var loadingopt = page.FindChildByXPath("//*[@class='select2-results__option loading-results']");
  aqObject.CheckProperty(loadingopt, "outerHTML", cmpEqual, "<li class=\"select2-results__option loading-results\" role=\"treeitem\" aria-disabled=\"true\">Searching…</li>");
  aqObject.CheckProperty(loadingopt, "contentText", cmpEqual, "Searching…");
  let searchBox = page.searchbox;
  aqObject.CheckProperty(searchBox, "Enabled", cmpEqual, true);
  searchBox.Click();
  searchBox.SetText("wellington");
});

Then("I should see Address Lookup info that matches my criteria", function (){
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  var addrset = page.FindChildByXPath("//*[@class = 'select2-results__options']")
  if (addrset.Exists){
    var addressoptn = page.EvaluateXPath("//li[@class='select2-results__option']")
    if(!strictEqual(addressoptn,null))
    addressoptn[1].Click();
  }
  else
  Log.Error("The address set is not loaded");
});

When("I enter more than one address", function (){
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  page.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.panel.textnode.Click();
  page.searchbox.SetText("upper hutt");
  var addressoptn = page.EvaluateXPath("//li[@class='select2-results__option']")
  if(!strictEqual(addressoptn,null))
  addressoptn[1].Click();
});

Then("both address should be visible on the Address location page", function (){
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove; 
  var addrlist = page.FindChildByXPath("//ul[@id='addressList']")
  Log.Message(addrlist);
  //if (addrlist.Exists){
    var selectedaddrs = page.EvaluateXPath("//*[@class='address-item full-address']")
    if(!strictEqual(selectedaddrs,null))
    Log.Message("Both address are :" + selectedaddrs[0].textContent + "\n" + selectedaddrs[1].textContent);
 //}
 else
 Log.Error("Address are not visible");
});

Then("I should be able to remove the address that I no longer require", function (){
   let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
    var removeaddrs = page.EvaluateXPath("//*[@class='remove']")
    if(!strictEqual(removeaddrs,null))
    removeaddrs[1].Click();
});

When("I submit my Address Lookup information", function (){
  let button = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.form.buttonNext;
  aqObject.CheckProperty(button, "contentText", cmpEqual, "Next");
  button.ClickButton();
});

Then("I should be taken to the Outcome Page successfully with suitable program details", function (){
  aqObject.CheckProperty(Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel.textnode, "contentText", cmpContains, "You have completed the tool. Your outcome is");
  WebTesting.WIN2DOutcome.Check();
  let panel = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel;
  var outcomeverify = panel.FindChildByXPath("//*[@id='main-content']")
  var outcomes = panel.FindChildByXPath("//div[@class=' win2d_outcomes']")
  var outcomeprocess = panel.FindChildByXPath("//div[@class='process-flow expandable']")
  if((outcomeverify.Exists) && (outcomes.Exists) && (outcomeprocess.Exists))
  Log.Message("Outcome page contains all required functionalities")
  else
  Log.Error("Outcome Page needs to be Verified");
});


Then("should be able to verify the Know Do Show Section and Scope of Operations Sections with Print button", function (){
  let panel = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove.panelPageWrapper.panel.panel.panel2.panel.panel.panel.panel.panel;
  let panel2 = panel.panel;
  aqObject.CheckProperty(panel2.panel.panel.link, "contentText", cmpEqual, "Print your outcome");
  Log.Message("Print button is available")
  var outcomeprocesssteps = panel.EvaluateXpath("//section[@class='process-step non-numbered processStep closed']")
  if (!strictEqual(outcomeprocesssteps,null))
  outcomeprocesssteps[0].Click();
  var outcomesection = panel.EvaluateXPath("//div[@class='win2d_output_section_category']")
  if(!strictEqual(outcomesection,null)){
  var knowsec = panel.EvaluateXPath("//*[@class='foodPlanIcon__icon foodPlanIcon__icon--know']")
  var dosec = panel.EvaluateXPath("//*[@class='foodPlanIcon__icon foodPlanIcon__icon--do']")
  var showsec = panel.EvaluateXPath("//*[@class='foodPlanIcon__icon foodPlanIcon__icon--do']")
  if((!strictEqual(knowsec,null)) && (!strictEqual(dosec,null)) && (!strictEqual(showsec,null)))
  Log.Message("Know Do and Show Section are available")
  }
  else
  Log.Error("My Plan section is not available");
  outcomeprocesssteps[1].scrollIntoView(true);
  outcomeprocesssteps[1].Click();
  var soosection = panel.EvaluateXPath("//*[@class='win2d_outcome win2d_outcome-soo']")
  if(!strictEqual(soosection,null))
  Log.Message("Scope Of Operation Section is available")
  else
  Log.Error("Scope of Operation is not available")
  let page = Aliases.browser.pageFoodSafetyNzFoodSafetyNzGove;
  var restart = panel.FindChildByXPath("//a[@class='win2d_navigation-restart__link']")
  restart.Click();
  page.Wait();
});

When("I click on Pig Count calculator tab", function (){
  let panel = Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel;
  aqObject.CheckProperty(panel.linkPigscountTab, "idStr", cmpEqual, "pigsCount-tab");
  panel.textnodeMytab.ClickTab("Pigs Count calculator title");
});

Then("I should be able to see three tiles namely area, weight and result as number of pigs", function (){
  let panel = Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel;
  var pigspacecalc = panel.FindChildByXPath("//*[@id ='pigSpaceCalculator']")
  if (pigspacecalc.Exists){
  let numberInput = panel.numberinputFreespace1;
  aqObject.CheckProperty(numberInput, "idStr", cmpEqual, "freeSpace1");
  aqObject.CheckProperty(numberInput, "value", cmpEqual, "0");
  let panel2 = panel.panelPigscount;
  numberInput = panel2.panel.numberinputMaxpigweight;
  aqObject.CheckProperty(numberInput, "idStr", cmpEqual, "maxPigWeight");
  aqObject.CheckProperty(numberInput, "value", cmpEqual, "0");
  panel = panel2.panelPigscountresult;
  aqObject.CheckProperty(panel, "idStr", cmpEqual, "pigsCountResult");
  aqObject.CheckProperty(panel, "contentText", cmpEqual, "0\nPigs");
  }
  else
  Log.Error("Pig Space Calculator tiles doesn't exist");
});

When("I enter the details in area tile and weight tile", function (){
  let panel = Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel;
  let numberInput = panel.numberinputFreespace1;
  numberInput.Click();
  numberInput.Keys("[BS]10");
  numberInput = panel.panelPigscount.panel.numberinputMaxpigweight;
  numberInput.Click();
  numberInput.Keys("[BS]10");
});

Then("It should calculate automatically and give me number of pigs result", function (){
  aqObject.CheckProperty(Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panelPigscount.panelPigscountresult, "contentText", cmpEqual, "71\nPigs");
});

When("I enter details in anyone tile either area or weight and not both", function (){
  let numberInput = Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.numberinputFreespace1;
  numberInput.Click();
  numberInput.SetText("0");
});


Then("It should give me the result as zero for number of pigs", function (){
  aqObject.CheckProperty(Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panelPigscount.panelPigscountresult, "contentText", cmpEqual, "0\nPigs");
});


When("I click on weight calculator tab", function (){
  let panel = Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel;
  aqObject.CheckProperty(panel.linkPigsweightTab, "idStr", cmpEqual, "pigsWeight-tab");
   //panel.textnodeMytab.ClickTab("wgt title");
   panel.linkPigsweightTab.Click();
  
});

Then("I should be able to see three tiles namely area, count of pigs and result as weight of pigs", function (){
  let panel = Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel;
  var pigspacecalc = panel.FindChildByXPath("//*[@id ='pigSpaceCalculator']")
  if (pigspacecalc.Exists){
  let numberInput = panel.numberinputFreespace2;
  aqObject.CheckProperty(numberInput, "idStr", cmpEqual, "freeSpace2");
  aqObject.CheckProperty(numberInput, "value", cmpEqual, "0");
  let panel2 = panel.panelPigsweight;
  numberInput = panel2.panel.numberinputMaxpignumber;
  aqObject.CheckProperty(numberInput, "idStr", cmpEqual, "maxPigNumber");
  aqObject.CheckProperty(numberInput, "value", cmpEqual, "0");
  panel = panel2.panelPigsweightresult;
  aqObject.CheckProperty(panel, "idStr", cmpEqual, "pigsWeightResult");
  aqObject.CheckProperty(panel, "contentText", cmpEqual, "0\nKg");
  }
   else
  Log.Error("Pig Space Calculator tiles doesn't exist");
});

When("I enter the details in area tile and count tile", function (){
  let panel = Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel;
  let numberInput = panel.numberinputFreespace2;
  numberInput.Click();
  numberInput.SetText("10");
  numberInput = panel.panelPigsweight.panel.numberinputMaxpignumber;
  numberInput.Click();
  numberInput.SetText("77");
});

Then("It should calculate automatically and give me weight of pigs result", function (){
  aqObject.CheckProperty(Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panelPigsweight.panelPigsweightresult, "contentText", cmpEqual, "8.91\nKg");
});

When("I enter details in anyone tile either area or count of pigs", function (){
  let numberInput = Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panelPigsweight.panel.numberinputMaxpignumber;
  numberInput.Click();
  numberInput.SetText("");
});

Then("It should give me the result as zero for weight of pigs", function (){
  aqObject.CheckProperty(Aliases.browser.pageAtTestPigSpaceCalculatorMpiN.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panelPigsweight.panelPigsweightresult, "contentText", cmpEqual, "0\nKg");
});

Then("I should be able to see Select Fish Stock Dropdown and Fish Stock Breakdown of Cost Recovery table", function (){
  let panel = Aliases.browser.pageFisheriesLevyCostRecoveryPag.panelPageWrapper.panel.panel.panel;
  let form = panel.formFormFishstockselectorform;
  aqObject.CheckProperty(form, "idStr", cmpEqual, "Form_FishstockSelectorForm");
  aqObject.CheckProperty(form, "contentText", cmpContains, "Select fish stock");
  aqObject.CheckProperty(panel.panel.panel.panel.panel.panel.panel.panel, "className", cmpEqual, "flcr_costs_panel");
});

When("I select Fish Stock with no data breakdown for Research", function (){
  let page = Aliases.browser.pageFisheriesLevyCostRecoveryPag;
  let textNode = page.panelPageWrapper.panel.panel.panel.formFormFishstockselectorform.fieldset.textnodeSelect2FormFishstocksele;
  textNode.Click();
  var anc2 = page.FindChildByXPath("//*[contains(@id,'ANC2')]")
  anc2.Click();
  page.Wait();
});

Then("I should be able to see three tables with Levy Share, Breakdown of Cost Recovery and Breakdown for Research category", function (){
  let panel = Aliases.browser.pageFisheriesLevyCostRecoveryPag.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel;
  aqObject.CheckProperty(panel.panel2, "className", cmpEqual, "flcr_proposal_panel");
  aqObject.CheckProperty(panel.panel, "className", cmpEqual, "flcr_costs_panel");
  aqObject.CheckProperty(panel.panel3, "className", cmpEqual, "flcr_projects_panel");
});

Then("Research Breakdown table should not have any data", function (){
  aqObject.CheckProperty(Aliases.browser.pageFisheriesLevyCostRecoveryPag.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel3.table.cell, "contentText", cmpEqual, "No proposed projects");
});

When("I select Fish Stock with data breakdown for Research", function (){
 let page = Aliases.browser.pageFisheriesLevyCostRecoveryPag;
  let textNode = page.panelPageWrapper.panel.panel.panel.formFormFishstockselectorform.fieldset.textnodeSelect2FormFishstocksele;
  textNode.Click();
  var bco2 = page.FindChildByXPath("//*[contains(@id,'BCO2')]")
  bco2.Click();
  page.Wait(); 
});

Then("Proposed Cost Recovery data for Research should be equal to the total in Breakdown for Research table", function (){
  panel = Aliases.browser.pageFisheriesLevyCostRecoveryPag.panelPageWrapper.panel.panel.panel.panel.panel.panel.panel.panel.panel;
  var res_totcost = panel.FindChildByXPath("//*[td='Research']/td[3]")
  var a = res_totcost.contentText;
  var totcost = panel.FindChildByXPath("//div[@class='flcr_projects_panel']/table/tfoot/tr/td[2]")
  if (totcost.contentText == a)
  Log.Message("The Totals are equal");
  else
  Log.Error("The totals are not equal.. Check!")
});

When("I scroll to the footer containing Social Media Icons", function (){
  let textNode = Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel.section.panel.textnode;
  textNode.scrollIntoView(true);
  aqObject.CheckProperty(textNode, "contentText", cmpEqual, "Follow us");
});

Then("I could see that each icon displays its respective tooltip", function (){
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel.section.panel.link, "title", cmpEqual, "MPI Twitter page.");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel.section.panel.link2, "title", cmpEqual, "MPI YouTube page.");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel.section.panel.link3, "title", cmpEqual, "MPI LinkedIn page.");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel.section.panel.link4, "title", cmpEqual, "MPI Facebook page.");
});

When("I click on {arg}", function (param1){
  let browser = Aliases.browser;
  if (param1 == "Twitter"){
  let link = browser.pageHome.panelBanner.panel.panel.panel2.panel.section.panel.link;
  aqObject.CheckProperty(link, "href", cmpEqual, "https://twitter.com/MPI_NZ");
  link.Click();
  //Sometimes MPI user Acceptance window pops up. Unable to automate because unable to detect that window due to the inconsistency
  let table = Aliases.browser.pageAcceptableUsagePolicy.panel.form.table.table;
  if (table.Exists){
  aqObject.CheckProperty(table.cell, "contentText", cmpEqual, "Acceptable Use Policy (AUP) Agreement");
  let button = table.buttonIAccept;
  aqObject.CheckProperty(button, "value", cmpEqual, "I Accept");
  button.ClickButton();
  }
  browser.pageMpiNz.Wait();
  }
  if (param1 == "Youtube"){
  let link = browser.pageHome.panelBanner.panel.panel.panel2.panel.section.panel.link2;
  aqObject.CheckProperty(link, "href", cmpEqual, "http://www.youtube.com/minprimaryindustries");
  link.Click();
  browser.pageMinprimaryindustries.Wait(); 
  }
  if (param1 == "LinkedIn"){
  let link = Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel.section.panel.link3;
  aqObject.CheckProperty(link, "href", cmpEqual, "http://www.linkedin.com/company/new-zealand-ministry-for-primary-industries");
  link.Click();
  browser.pageNewZealandMinistryForPrimary.Wait();  
  }
  if (param1 == "Facebook"){
  let link = browser.pageHome.panelBanner.panel.panel.panel2.panel.section.panel.link4;
  aqObject.CheckProperty(link, "href", cmpEqual, "https://www.facebook.com/MPIgovtnz");
  link.Click();
  browser.pageMpigovtnz.Wait();
  }
  Log.Message(param1 +" icon is clicked");
});

Then("I should be able to Navigate to the respective {arg} page", function (param1){
    // Obtains the page currently opened in the browser
  var page = Aliases.browser.Page("*");
  // Posts the URL of the currently open page to the log
  Log.Message(page.URL);
  if (param1 == "Twitter"){
    var url = "https://twitter.com/MPI_NZ";
  }
  else if (param1 == "Youtube"){
    var url = "https://www.youtube.com/user/MinPrimaryIndustries";
  } 
  else if (param1 == "LinkedIn"){
    var url = "http://www.linkedin.com/company/new-zealand-ministry-for-primary-industries";
  }
  else if (param1 == "Facebook"){
    var url = "https://www.facebook.com/MPIgovtnz";
  }
    aqString.StrMatches(page.URL,url);
    Log.Message("Social Media Page has been successfully navigated");
    page.Close(); 
});

When("I click on the Hamburger menu", function (){
  let svg = Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.buttonToggleNavigation.vgPrimarynavbuttonicon;
  aqObject.CheckProperty(svg, "idStr", cmpEqual, "PrimaryNavButtonIcon");
  svg.Click();
});

When("I click on the topic in the menu list", function (){
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.navPrimarynav.textnodePrimarynavmenu, "className", cmpEqual, "primary-nav-dropdown");
  let menu = Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.navPrimarynav.textnodePrimarynavmenu;
  var button1 = menu.FindChildByXPath("//*[@data-canonical='protection-and-response/']/following-sibling::button")
  button1.click();
  Delay(3000);
  var button2 = menu.FindChildByXPath("//*[@data-canonical='protection-and-response/biosecurity/']/following-sibling::button")
  button2.click();
  Delay(3000);
  //var link = menu.FindChildByXPath("//a[@href='https://mpigovtnz-test1.cwp.govt.nz/protection-and-response/biosecurity/biosecurity-2025/']")
  //Log.Message(link.innerText);
  //link.Click();
  Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.navPrimarynav.link.Click();
  Aliases.browser.pageMpigovtnzTest1CwpGovtNzProte.Wait();  
});

Then("I should be navigated to the required Page", function (){
  var url = "https://mpigovtnz-test1.cwp.govt.nz/protection-and-response/biosecurity/biosecurity-2025/"
  var page = Aliases.browser.Page("*");
  aqString.StrMatches(page.URL,url);
  Log.Message("The page was properly navigated"); 
});

When("I click on Hamburger Menu on the navigated Page", function (){
  let svg = Aliases.browser.pageMpigovtnzTest1CwpGovtNzProte.header.panel.panel.panel.panel.panel.panel.panel.buttonToggleNavigation.vgPrimarynavbuttonicon;
  aqObject.CheckProperty(svg, "idStr", cmpEqual, "PrimaryNavButtonIcon");
  svg.Click();
});

Then("I should be able to see the page menu being expanded", function (){
  aqObject.CheckProperty(Aliases.browser.pageMpigovtnzTest1CwpGovtNzProte.header.panel.panel.panel.panel.panel.panel.panel.navPrimarynav.textnodePrimarynavmenu, "className", cmpEqual, "primary-nav-dropdown");
  let menu = Aliases.browser.pageMpigovtnzTest1CwpGovtNzProte.header.panel.panel.panel.panel.panel.panel.panel.navPrimarynav.textnodePrimarynavmenu;
  var link1 = menu.FindChildByXPath("//a[@href='https://mpigovtnz-test1.cwp.govt.nz/protection-and-response/biosecurity/biosecurity-2025/']")
  aqObject.CheckProperty(link1, "className", cmpEqual, "navItem__current");
  Log.Message("The Hamburger Menu is expanded and points to the current page")
});

When("I see the HomePage content header with branch navigation", function (){
  WebTesting.CWPHomePage.Check();
  aqObject.CheckProperty(Aliases.browser.pageHome.header, "idstr", cmpEqual, "header");
  var branch_nav = Aliases.browser.pageHome.EvaluateXPath("//header//div[@class='branch-master-wrap']/div/div[@class='branch-master-col']") 
  if (!strictEqual(branch_nav,null))
  Log.Message("Branch-Navigation panel exists");
  else
  Log.Error("Branch-Nav doesn't exists")
});

Then("I should see the {arg} tiles", function (param1){
  if(param1 == "MPI Logo"){
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.link, "href", cmpEqual, "https://mpigovtnz-test1.cwp.govt.nz/")
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.imageMpiLogoSvg, "src", cmpEqual, "https://mpigovtnz-test1.cwp.govt.nz/themes/2018/images/mpi-logo.svg");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.imageMpiLogoSvg, "namePropStr", cmpEqual, "mpi-logo.svg");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.imageMpiLogoSvg, "Exists", cmpEqual, true);
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.imageMpiLogoSvg, "Name", cmpEqual, "Image(\"mpi_logo_svg\")");
  }
  if(param1 == "MPI Home"){
  aqObject.CheckProperty(Aliases.browser.pageHome.header.navUtilitynav.link, "Exists", cmpEqual, true);
  aqObject.CheckProperty(Aliases.browser.pageHome.header.navUtilitynav.link, "href", cmpEqual, "https://mpigovtnz-test1.cwp.govt.nz/");
  aqObject.CheckProperty(Aliases.browser.pageHome.header.navUtilitynav.link, "title", cmpEqual, "MPI Home");
  }
  if(param1 == "Logout"){
  aqObject.CheckProperty(Aliases.browser.pageHome.header.navUtilitynav.link2, "Exists", cmpEqual, true);
  aqObject.CheckProperty(Aliases.browser.pageHome.header.navUtilitynav.link2, "contentText", cmpEqual, "Logout");
  aqObject.CheckProperty(Aliases.browser.pageHome.header.navUtilitynav.link2, "href", cmpEqual, "https://mpigovtnz-test1.cwp.govt.nz/Security/logout");
  }
  if(param1 == "Contact"){
  aqObject.CheckProperty(Aliases.browser.pageHome.header.navUtilitynav.link3, "Exists", cmpEqual, true);
  aqObject.CheckProperty(Aliases.browser.pageHome.header.navUtilitynav.link3, "contentText", cmpEqual, "Contact");
  aqObject.CheckProperty(Aliases.browser.pageHome.header.navUtilitynav.link3, "href", cmpEqual, "https://mpigovtnz-test1.cwp.govt.nz/contact-us/");
  }
  if(param1 == "Keyword Search"){
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.searchboxSiteSearch, "Enabled", cmpEqual, true);
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.searchboxSiteSearch, "Exists", cmpEqual, true);
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.searchboxSiteSearch, "Name", cmpEqual, "SearchBox(\"site_search\")");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.searchboxSiteSearch, "className", cmpEqual, "st-default-search-input");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.searchboxSiteSearch, "idStr", cmpEqual, "site-search");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.searchboxSiteSearch, "tagName", cmpEqual, "INPUT");
  }
  if(param1 == "Search icon"){
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.vgHeadersearchicon, "Enabled", cmpEqual, true);
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.vgHeadersearchicon, "Exists", cmpEqual, true);
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.vgHeadersearchicon, "Name", cmpEqual, "SVG(\"HeaderSearchIcon\")");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel.formPageHeadSearchForm.fieldset.vgHeadersearchicon, "idStr", cmpEqual, "HeaderSearchIcon");
  }  
  if(param1 == "Hamburger Menu"){
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.buttonToggleNavigation.vgPrimarynavbuttonicon, "idStr", cmpEqual, "PrimaryNavButtonIcon");
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.buttonToggleNavigation.vgPrimarynavbuttonicon, "Name", cmpEqual, "SVG(\"PrimaryNavButtonIcon\")");  
  }
});

When("I see panels for menu items, branch items, media releases, feature tiles and Promo tiles", function (){
  aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel2.panel2.panel_new.panel.panel, "className", cmpEqual, "homePopularLinks__menu-inner");
  var popular_menu = Aliases.browser.pageHome.panelBanner.EvaluateXPath("//*[contains(@class,'homePopularLinks__menu__item ')]")
  if(!strictEqual(popular_menu,null))
  Log.Message("Popular Menu exists")
  else
  Log.Error("Popular menu doesnot exists")
  if(aqObject.CheckProperty(Aliases.browser.pageHome.panelBanner.panel.panel.panel3.panel.panel.panel, "className", cmpEqual, "row branch-buttons-list") == true)
  Log.Message("The Branch list is visible")
  let browser = Aliases.browser;
  let section = browser.pageHome.panelPageWrapper.panel.panel.panel.section;
  if(aqObject.CheckProperty(section, "className", cmpEqual, "media-releases") == true)
  Log.Message("The Media releases panel exists")
  let panel = Aliases.browser.pageHome.panelPageWrapper;
  if(aqObject.CheckProperty(panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel.panel, "className", cmpEqual, "featureTiles") == true)
  Log.Message("Feature tiles are visible")
  if(aqObject.CheckProperty(panel.panel2, "className", cmpEqual, "layout__promoTiles layout__promoTiles--full")== true)
  Log.Message("Promo tiles are visible")
});
