﻿function CheckBrowser()
{
  var page = Sys.Browser("chrome").Page("*");
  if(page) Sys.Browser("chrome").Close();
  aqObject.CheckProperty(Aliases.browser.pageMailCwpMpiGovtNz.Confirm, "Message", cmpEqual, "");
 // aqObject.CheckProperty(Aliases.browser.pageMailCwpMpiGovtNz.Confirm, "ObjectType", cmpEqual, "Confirm");
  let button = Aliases.browser.pageMailCwpMpiGovtNz.Confirm.buttonOk;
  aqObject.CheckProperty(button, "Enabled", cmpEqual, true);
  button.ClickButton();
}

module.exports.CheckBrowser = CheckBrowser;

function getArraylength(){
  var panel, docarr;
  panel = Aliases.browser.pagePublicationsMpiNzGovernment.panelPageWrapper.panel.panel_new.panelDocumentSet7.panelArticleList7;
  docarr = panel.EvaluateXPath("//article");
  return docarr.length; 
}
//without the below line, the function won't be used in other script files
module.exports.getArraylength = getArraylength;