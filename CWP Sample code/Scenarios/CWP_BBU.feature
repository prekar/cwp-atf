﻿Feature: Regression Test Pack of CWP Functionality

Scenario Outline: To test the header functionality of CWP Home Page
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
When I see the HomePage content header with branch navigation
Then I should see the "<Header>" tiles

Examples:
|Header|            
|MPI Logo|          
|MPI Home|          
|Logout|            
|Contact|           
|Keyword Search|    
|Search icon|       
|Hamburger Menu|  

Scenario: To test the different panels of the CWP home page  
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
When I see panels for menu items, branch items, media releases, feature tiles and Promo tiles
#Then I will be able to see only eight feature tiles and four promo tiles

Scenario Outline: To test the footer functionality of CWP Home Page
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
#When I go to the footer panel ans see Sections, Updates and Contact Us
#Then I should be able to see all the "<Footer>" links

Examples:
|Footer|
|MPI Logo|
|Shield Badge|
|NZ Govt Logo|
|Disclaimer|
|Sitemap| 
|Copyright|
|Privacy and Security|

Scenario Outline: Check the Branding Page Navigation
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
When I click the "<BBU unit>" tile
Then the "<BBU unit>" page is being displayed

Examples:
|BBU unit|
|Biosecurity|
|Fisheries|
|Food Safety|
|Forestry|
|AIS|

Scenario: Check the Subscribe to MPI functionality by updating the list
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
And I click Subscribe to MPI
When I enter the Email and click Manage Subscriptions
And I receive an email for subscription
Then I click on Subscription link
And I subscribe and update to topics of my choice
When I click on Confirmation link sent to my email
Then I see the subscription has successfully updated

#Scenario Outline: To check the unsubscribe functionality
#Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
#And I click Subscribe to MPI
#When I enter the "<Email>" and click Manage Subscriptions
#And I receive an "<Email>" for subscription
#Then I click on Subscription link
#And I unsubscribe from all topics
#When I click on confirmation link sent to my "<Email>"
#Then I see that I am unsubscribed from all the list

#Examples:
#|Email|
#|abc@xyz.com|

Scenario Outline: Check the feedback Form functionality through feedback bubble
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
And I have navigated to "<Page type>" page
When I click on the Feedback bubble
And when I enter the Feedback form details
Then I close the Feedback form pop-up
#Then I can see Thank you for feedback page

Examples:
|Page type|
|Food Safety|

Scenario Outline: To test the functionality of Expandable Page
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
And I have navigated to "<Page type>" page
When I click on each Expandable Item
Then I should be able to view the content inside the expandable
When I click on Expand All button
Then all the Expandable Items should be expanded
When I Click on Collapse All buuton
Then all the opened expandable items should close
When I click on a topic in Bookmark link
Then it should take me to the related Expandable Set 
#The Scroll function of the page still needs to be verified

Examples:
|Page type|
|Expandable Page|

Scenario Outline: To test the functionality of Document Set
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
And I have navigated to "<Page type>" page
When I click anywhere in the title to open a document
Then the document content should be expanded
And clicking again should close the expanded content
When I click the Download button
Then the document should open in new tab
When I click Load more 
Then the document set items are loaded
When I click Expand All button in Doc set
And I click Load more 
Then the document set items that are loaded is also expanded
When I search for a document using valid Keyword and click Submit
Then it should result in the list of documents matching the Keyword 
When I click Reset
Then the Search field should be empty
When I search for a document using invalid Keyword and click Submit
Then it should not populate any results 

Examples:
|Page type|
|Docset|

Scenario Outline: To test the functionality of User Forms
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
And I have navigated to "<Page type>" page
When I enter the related details and Click Clear form 
#This contains all form functionalities like upload,radio box, checkbox, text field 
Then I should be able to see that all the data are erased
When I enter mandatory field details
And I delete the mandatory field
And I click Submit button
Then I should be able to see the error message
When I enter mandatory field details
And I click Submit button
Then I should be able to successfully submit the form 

Examples:
|Page type|
|Userforms|

Scenario: To test the functionality of Media Releases
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
When I scroll to Media Releases section
Then I could see only three Media releases tile arranged by date and Category
And the abstract of the Media releases tile are trimmed to three dots after hundred characters
When I click on View All Media Releases
Then I could see a set of Media Releases topic arranged by Date with a Show more link
When I click the Show more link 
Then other set of Media Release topics are loaded
When I click on a topic to filter media Releases by Category
Then I could see only the Media Release topics related to the category are displayed

Scenario Outline: To test WIN2D functionality
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
And I have navigated to "<Page type>" page
When I click on Start Page 
Then I should be taken to WIN2D journey and see the progress bar start point
And I should be able to see the downward arrow to expand the question tile
When I click Next button
Then I should be presented with error page
When I see multi choice question
Then I should be able to select more than one answer
When I click Next button
And Navigate to single option question
Then I should be able to select only one answer
When I click on Review answers link from second question
Then I should be able to see the Review answers page with all the questions I have answered so far
When on the Review Page, I click reset
Then I should be taken to the question where I click Reset
When I click on Restart Questionnaire link
Then I should be taken to the First Question Page
When I answer all the questions of the WIN2D journey
And I could see Next button, back button, Review answers link, Restart Questionnaire link and the progress bar
Then I should be able to sucessfully submit all my answers in the Review page
When I go to Location Verification page
Then I should see that the Next button is not enabled eithout entering the address
When I enter location address
Then I should see Address Lookup info that matches my criteria
When I enter more than one address
Then both address should be visible on the Address location page
And I should be able to remove the address that I no longer require
When I submit my Address Lookup information
Then I should be taken to the Outcome Page successfully with suitable program details
And should be able to verify the Know Do Show Section and Scope of Operations Sections with Print button

Examples:
|Page type|
|WIN2D|

Scenario Outline: To test the functionality of Pig Space Calculator
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
And I have navigated to "<Page type>" page
When I click on Pig Count calculator tab
Then I should be able to see three tiles namely area, weight and result as number of pigs
When I enter the details in area tile and weight tile
Then It should calculate automatically and give me number of pigs result
When I enter details in anyone tile either area or weight and not both
Then It should give me the result as zero for number of pigs
When I click on weight calculator tab
Then I should be able to see three tiles namely area, count of pigs and result as weight of pigs
When I enter the details in area tile and count tile
Then It should calculate automatically and give me weight of pigs result
When I enter details in anyone tile either area or count of pigs
Then It should give me the result as zero for weight of pigs

Examples:
|Page type|
|Pig Space Calculator|

Scenario Outline: To test the functionality of Fisheries Levy Cost Recovery Page
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
And I have navigated to "<Page type>" page
Then I should be able to see Select Fish Stock Dropdown and Fish Stock Breakdown of Cost Recovery table
When I select Fish Stock with no data breakdown for Research
Then I should be able to see three tables with Levy Share, Breakdown of Cost Recovery and Breakdown for Research category
And Research Breakdown table should not have any data
When I select Fish Stock with data breakdown for Research
Then I should be able to see three tables with Levy Share, Breakdown of Cost Recovery and Breakdown for Research category
And Proposed Cost Recovery data for Research should be equal to the total in Breakdown for Research table

Examples:
|Page type|
|FLCR Page|

Scenario Outline: To test the functionality of Social Media Icons
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
When I scroll to the footer containing Social Media Icons
Then I could see that each icon displays its respective tooltip
When I click on "<Social Media Icon>"
Then I should be able to Navigate to the respective "<Social Media Icon>" page

Examples:
|Social Media Icon|
|Twitter|
|Youtube|
|LinkedIn|
|Facebook|

Scenario: To test the functionality of Hamburger Menus
Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
When I click on the Hamburger menu
And I click on the topic in the menu list
Then I should be navigated to the required Page
When I click on Hamburger Menu on the navigated Page
Then I should be able to see the page menu being expanded

#Scenario Outline: To test whether the  branch logos are linked to right pages
#Given I have launched the CWP website "https://mpigovtnz-test1.cwp.govt.nz/"
#And  navigate to the page containing the "<Branch>"
#When I click on the "<Branch>" logo
#Then I should be taken the respective "<Branch>" landing page

#Examples:
#|Branch|
#|MPI|
#|Bio Security|
#|Forestry|
#|Food Safety|
#|Fisheries|
#|AIS| 


